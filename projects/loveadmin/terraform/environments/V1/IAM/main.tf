provider "aws" {
  region = "eu-west-2"
}

module "prod_adminrole" {
  source = "../../../modules/iam_role/"
  role_name = var.role_name
  policy_location = templatefile("policies/role-trust-policy.json", {})
  access_policy = var.access_policy
}

module "CloudWatch-RO" {
  source =  "../../../modules/iam_role/"
  role_name = var.role_name-CW
  policy_location = templatefile("policies/role-trust-policy.json", {})
  access_policy = var.access_policy-CW
}

resource "aws_iam_policy" "self-managemnet" {
  name        = "self-user-managment-policy"
  description = "This policy allows users to manage their own atributes"
  policy      = templatefile("policies/self-user-managment-policy.json", {})
}
