variable "role_name" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-Admin"
}

variable "access_policy" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/AdministratorAccess"
}

variable "role_name-CW" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-CloudWatchReadOnly"
}

variable "access_policy-CW" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}
