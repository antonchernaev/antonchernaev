### uncomment this after you create the bucket and run terraform init again
terraform {
   backend "s3" {
     bucket = "s3statebucketloveadmin"
     key    = "V1/S3/terraform.tfstate"
     region = "eu-west-2"
   }
 }
