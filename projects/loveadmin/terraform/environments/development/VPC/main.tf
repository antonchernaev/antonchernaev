provider "aws" {
  region = var.AWS_REGION
}

module "vpc_and_subnets" {
  source = "../../../modules/vpc"
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
  deployment_method = var.deployment_method
  vpc_flowlogs_role_name = var.vpc_flowlogs_role_name
  vpc_flowlogs_policy_name = var.vpc_flowlogs_policy_name
  vpc_cloudwatch_log_group = var.vpc_cloudwatch_log_group
  vpc_subnet_cidr_private = var.vpc_subnet_cidr_private
  vpc_subnet_cidr_public = var.vpc_subnet_cidr_public
  subnet_description_private = var.subnet_description_private
  subnet_description_public = var.subnet_description_public
  number_of_subnets_private = var.number_of_subnets_private
  number_of_subnets_public = var.number_of_subnets_public
  vpc_elasticip = var.vpc_elasticip
  number_nat_gateways_private_subnets = var.number_nat_gateways_private_subnets
  vpc_flowLog_destination = var.vpc_flowlogs_s3
  route_table_description = var.route_table_description
}
