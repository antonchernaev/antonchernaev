variable "AWS_REGION" {
    default = "eu-west-2"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "172.28.0.0/16"

}

variable "vpc_name" {
    description = "Default Name of the VPC"
    default = "Development"
}

variable "deployment_method" {
    description = "Method for deployment"
    default = "terraform"
}

variable "vpc_flowlogs_role_name" {
    description = "Defult name for flowlogs IAM role"
    default = "vpc_flowlogs"
}

variable "vpc_flowlogs_policy_name" {
  description = "Default Name for flowlogs IAM policy"
  default = "vpc_flowlogs_policy"
}

variable "vpc_cloudwatch_log_group" {
    description = "Default Name for the Cloudwatch logs log group"
    default = "vpc_flow_logs"
}

variable "vpc_subnet_cidr_private" {
  description = "CIDR block for private vpc subnets"
  type    = list(string)
  default = ["172.28.0.0/20", "172.28.16.0/20"]
}

variable "vpc_subnet_cidr_public" {
  description = "CIDR block for public vpc subnets"
  type    = list(string)
  default = ["172.28.32.0/20", "172.28.48.0/20"]
}

variable "subnet_description_private" {
  description = "shared services, public, 1 or 2 or .."
  default = ["Development-private-1", "Development-private-2"]
}

variable "subnet_description_public" {
  description = "shared services, public, 1 or 2 or .."
  default = ["Development-public-1", "Development-public-2"]
}

variable "route_table_description" {
  description = "shared services, public, 1 or 2 or .."
  default = ["Development-public-rt", "Development-private-rt"]
}

variable "number_of_subnets_private" {
  description = "Number of subnets to create."
  type = number
  default = 2
}

variable "number_of_subnets_public" {
  description = "Number of subnets to create."
  type = number
  default = 2
}

variable "vpc_elasticip" {
  description = "Elastic ip"
  type = bool
  default = true
}

variable "number_nat_gateways_private_subnets" {
  description = "Number of NAT Gateways"
  default = 1
}

variable "vpc_flowlogs_s3" {
  description = "S3 bucket where flow logs will be stored"
  default = "arn:aws:s3:::loveadmin-vpc-flowlogs"
}
