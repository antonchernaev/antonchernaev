provider "aws" {
  region = "eu-west-2"
}

data "aws_organizations_organization" "current" {}

module "s3_access_loging" {
  source = "../../../../modules/s3_bucket"
  bucket-name = var.bucket_name_s3_access_logs
  policy_doc = templatefile("policies/s3_access_logging_bucket_cloudtrail_allow_policy.json", {bucket_name = var.bucket_name_s3_access_logs, org_id = data.aws_organizations_organization.current.id})
  acl_access_logging = "log-delivery-write"
  enable_logging = false
}


module "s3_state" {
  source = "../../../../modules/s3_bucket"
  bucket-name = var.s3_state_bucket
  policy_doc = null
  bucket_name_s3_access_logs = var.bucket_name_s3_access_logs
  target_prefix = var.target_prefix_s3_state_bucket

}

module "s3_cloudtrail" {
  source = "../../../../modules/s3_bucket"
  bucket-name = var.bucket_name_cloud_trail
  policy_doc = templatefile("policies/CloudTrailEventPolicyAWSorganization.json", {bucket_name = var.bucket_name_cloud_trail, org_id = data.aws_organizations_organization.current.id})
  bucket_name_s3_access_logs = var.bucket_name_s3_access_logs
  target_prefix = var.target_prefix_cloud_trail

}

module "s3_vpc_flowlogs" {
  source = "../../../../modules/s3_bucket"
  bucket-name = var.bucket_name_vpc_flowlogs
  policy_doc = templatefile("policies/s3_vpc_flowlogs-AllowBucketPolicy.json", {bucket_name = var.bucket_name_vpc_flowlogs, org_id = data.aws_organizations_organization.current.id})
  bucket_name_s3_access_logs = var.bucket_name_s3_access_logs
  target_prefix = var.target_prefix_vpc_flowlogs
}








