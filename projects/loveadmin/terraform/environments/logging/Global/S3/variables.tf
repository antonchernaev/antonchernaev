variable "bucket_name_vpc_flowlogs" {
  description = "Bucket for vpc flowlogs"
  default = "loveadmin-vpc-flowlogs"
}

variable "bucket_name_cloud_trail" {
  description = "cloud trail bucket"
  default = "loveadmin-cloudtrail-logs"
}

variable "bucket_name_s3_access_logs" {
  description = "Bucket for collecting s3 object events"
  default = "loveadmin-s3-access-logs"
}


variable "s3_state_bucket" {
  description = "The bucket to use for storing terrform state files"
  default = "loveadmin-terraform-state-logging"
}

variable "target_prefix_vpc_flowlogs" {
  description = "bucket log directory"
  default = "loveadmin-vpc-flowlogs/"
}

variable "target_prefix_cloud_trail" {
  description = "bucket log directory"
  default = "loveadmin-cloudtrail-logs/"
}

variable "target_prefix_s3_state_bucket" {
  description = "bucket log directory"
  default = "loveadmin-terraform-state-logging/"
}
