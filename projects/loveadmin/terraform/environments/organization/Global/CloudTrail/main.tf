provider "aws" {
  region = var.region
}

resource "aws_iam_policy" "CloudTrail_policy" {
  name = "cloudTrail_Role_access_policy"
  description = "CloudTrail access policy"
  policy = templatefile("policies/Policy_permissions_CloudWatch_CloudTrail_role.json", {})
}

module "CloudTrail_to_CloudWatch" {
  source = "../../../../modules/iam_role"
  role_name = var.role_name
  policy_location = templatefile("policies/Assume_CloudWatch_role_CloudTrail.json", {})
  access_policy = aws_iam_policy.CloudTrail_policy.arn
}

resource "aws_cloudwatch_log_group" "CloudTrail_send_log_group" {
  name = var.CloudWatch_log_group
  tags = {
    Environment = "production"
    Service = "CloudTrail"
  }
}

resource "aws_cloudtrail" "main" {
  depends_on = [module.CloudTrail_to_CloudWatch]
  name = var.cloudtrail_logs
  enable_logging = true
  s3_bucket_name = var.s3_cloudtrail_bucket
  enable_log_file_validation = true
  is_multi_region_trail = true
  include_global_service_events = true
  is_organization_trail = true
  cloud_watch_logs_role_arn = module.CloudTrail_to_CloudWatch.role_arn
  cloud_watch_logs_group_arn = aws_cloudwatch_log_group.CloudTrail_send_log_group.arn

}
