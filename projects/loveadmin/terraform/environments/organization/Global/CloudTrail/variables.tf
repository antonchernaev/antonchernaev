variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-2"
}

//variable "s3_object_logging_S3s" {
//  description = "name of the cloudtrail bucket"
//  default = "s3objectloggingbucketloveadmin"
//}

variable "cloudtrail_logs" {
  description = "cloudtrail for AWS organization events"
  default = "cloudtrail-logs"
}

variable "s3_cloudtrail_bucket" {
  description = "name of the cloudtrail bucket"
  default = "loveadmin-cloudtrail-logs"
}

variable "s3_access_loging" {
  description = "name of the cloudtrail bucket"
  default = "loveadmin-s3-access-logs"
}

variable "CloudWatch_log_group" {
  description = "Log group for CloudWatch"
  default = "CloudTrail"
}

variable "role_name" {
  type = string
  description = "AWS IAM Role CloudTrail"
  default = "CloudTrail_CloudWatchLogs_Role_OrgTrail"
}

#variable "policy_location" {
#  description = "Define assume Policy location"
#  default = "policies/Assume_CloudWatch_role_CloudTrail.json"
#}

#variable "access_policy" {
#  type = string
#  description = "arn of the access policy"
#  default = "policies/Policy_permissions_CloudWatch_CloudTrail_role.json"
#}
