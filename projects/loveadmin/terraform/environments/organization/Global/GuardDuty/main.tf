// Organizations Master Account
resource "aws_guardduty_detector" "master" {
  enable = true

  finding_publishing_frequency = var.frequency
}

// Creating GurardDuty membership for the rest of the accounts

resource "aws_guardduty_detector" "dev" {
  provider = aws.dev
  enable   = true
}

module "GuardDuty_Dev" {

  providers = {
    aws.master = aws
    aws.member = aws.dev
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["dev"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.dev.id
}


resource "aws_guardduty_detector" "logging" {
  provider = aws.logging
  enable   = true
}

module "GuardDuty_logging" {

  providers = {
    aws.master = aws
    aws.member = aws.logging
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["log"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.logging.id
}


resource "aws_guardduty_detector" "shs" {
  provider = aws.shs
  enable   = true
}

module "GuardDuty" {

  providers = {
    aws.master = aws
    aws.member = aws.shs
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["shs"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.shs.id
}

resource "aws_guardduty_detector" "prod" {
  provider = aws.prod
  enable   = true
}

module "GuardDuty_prod" {

  providers = {
    aws.master = aws
    aws.member = aws.prod
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["prod"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.prod.id
}

resource "aws_guardduty_detector" "stg" {
  provider = aws.stg
  enable   = true
}

module "GuardDuty_stg" {

  providers = {
    aws.master = aws
    aws.member = aws.stg
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["stg"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.stg.id
}

resource "aws_guardduty_detector" "v1" {
  provider = aws.v1
  enable   = true
}

module "GuardDuty_v1" {

  providers = {
    aws.master = aws
    aws.member = aws.v1
  }

  source             = "../../../../modules/GuardDuty"
  email              = var.acc_emails["v1"]
  detector_master_id = aws_guardduty_detector.master.id
  detector_member_id = aws_guardduty_detector.v1.id
}

