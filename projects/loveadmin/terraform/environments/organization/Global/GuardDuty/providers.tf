provider "aws" {
  region = var.region
  alias  = "dev"
  assume_role {
    role_arn = var.acc_roles["dev"]
  }
}


provider "aws" {
  region = var.region
}

provider "aws" {
  region = var.region
  alias  = "logging"
  assume_role {
    role_arn = var.acc_roles["log"]
  }
}


provider "aws" {
  region = var.region
  alias  = "shs"
  assume_role {
    role_arn = var.acc_roles["shs"]
  }
}

provider "aws" {
  region = var.region
  alias  = "prod"
  assume_role {
    role_arn = var.acc_roles["prod"]
  }
}


provider "aws" {
  region = var.region
  alias  = "stg"
  assume_role {
    role_arn = var.acc_roles["stg"]
  }
}

provider "aws" {
  region = var.region
  alias  = "v1"
  assume_role {
    role_arn = var.acc_roles["v1"]
  }
}

