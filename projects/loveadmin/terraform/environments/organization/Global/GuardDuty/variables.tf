variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-2"
}

variable "frequency" {
  type        = string
  description = "Specifies the frequency of notifications sent for subsequent finding occurrences."
  // Allowed values: FIFTEEN_MINUTES, ONE_HOUR, SIX_HOURS
  // If the provided value differs from allowed ones or is omitted, it will be defaulted to 6h!
  default = "ONE_HOUR"
}

variable "acc_emails" {
  type = map
  default = {
    dev = "aws_dev_env@loveadmin.com"
    log = "aws_logging@loveadmin.com"
    shs = "aws_shared_services@loveadmin.com"
    prod = "aws_prod_env@loveadmin.com"
    stg = "aws_staging_env@loveadmin.com"
    v1 = "dave@paysubsonline.com"
  }
}

variable "acc_roles" {
  type = map
  default = {
    dev = "arn:aws:iam::761632467185:role/OrganizationAdminRole"
    log = "arn:aws:iam::068325409561:role/OrganizationAdminRole"
    shs = "arn:aws:iam::788038158421:role/OrganizationAdminRole"
    prod = "arn:aws:iam::615990240101:role/OrganizationAdminRole"
    stg = "arn:aws:iam::526641343317:role/OrganizationAdminRole"
    v1 = "arn:aws:iam::729481545206:role/OrganizationAdminRole"
  }
}
