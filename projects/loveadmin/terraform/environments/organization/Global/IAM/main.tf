provider "aws" {
  region = var.region
}

module "iam_settings" {
  source      = "../../../../modules/iam_settings"
  min_pass_lenght_module = var.min_pass_lenght
  expiration_days = var.expiration_days
  pass_remember = var.pass_remember
}

########### IAM Group Creation ###########

resource "aws_iam_group" "users_groups" {
  count = length(var.organization_group)
  name = var.organization_group[count.index]
}

resource "aws_iam_policy" "self-managemnet" {
  name        = "self-user-managment-policy"
  description = "This policy allows users to manage their own atributes"
  policy      = templatefile("policies/self-user-managment-policy.json", {})
}

resource "aws_iam_policy" "assume-role" {
  name        = "admin-assume-role"
  description = "This policy allows users to assume all other roles"
  policy      = templatefile("policies/admin-assume-role.json", {})
}

resource "aws_iam_policy" "cloudwatch-assume-role" {
  name        = "cloudwatch-assume-role"
  description = "This policy allows users to assume CloudWatchRole role"
  policy      = templatefile("policies/cloudwatch-assume-role.json", {})
}

resource "aws_iam_group_policy_attachment" "admin" {
  count = length(var.organization_group)
  group      = aws_iam_group.users_groups[count.index].name
  policy_arn = aws_iam_policy.self-managemnet.arn
}

resource "aws_iam_group_policy_attachment" "admin-assume-role" {
  group      = aws_iam_group.users_groups[0].name
  policy_arn = aws_iam_policy.assume-role.arn
}

resource "aws_iam_group_policy_attachment" "cloudwatch-assume-role" {
  group      = aws_iam_group.users_groups[2].name
  policy_arn = aws_iam_policy.cloudwatch-assume-role.arn
}

########### IAM User Creation ###########

module "admin_users" {
  source = "../../../../modules/iam_user"
  iam_group = aws_iam_group.users_groups[0].name
  user_names = var.admins
}

module "gillian_drillsma" {
  source = "../../../../modules/iam_user"
  iam_group = aws_iam_group.users_groups[1].name
  user_names = var.billing_users
}

module "cloudwatch_reports" {
  source = "../../../../modules/iam_user"
  iam_group = aws_iam_group.users_groups[2].name
  user_names = var.CloudWatch_users
}


########### IAM Role Creation ###########

module "iam_role_Admin" {
  source = "../../../../modules/iam_role"
  role_name = var.role_name
  policy_location = templatefile(var.policy_location, {})
  access_policy = var.access_policy
}

module "iam_role_CloudWatch" {
  source = "../../../../modules/iam_role"
  role_name = var.role_name_CW
  policy_location = templatefile(var.policy_location, {})
  access_policy = var.access_policy_CW
}


