######### IAM Config #########

variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-2"
}

variable "min_pass_lenght" {
  description = "Minimum lenght of the password"
  default = "8"
}

variable "policy_arn" {
  description = "Managed AWS policy to be attached"
  default     = ""
}

variable "expiration_days" {
  description = "Days before password expire"
  default     = "90"
}

variable "pass_remember" {
   description = "Number of remebered old user's passwords"
   default     = "5"
 }

######### IAM Users and Groups #########

variable "organization_group" {
  description = "Create IAM groups with these names"
  type = list(string)
  default = ["Admins", "Billing", "CloudWatch"]
}

variable "admins" {
  description = "Create IAM users with these names"
  type = list(string)
  default = ["Tim_Hancock","Dave_Evans"]
}

variable "billing_users" {
  description = "Create IAM users with these names"
  type = list(string)
  default = ["Gillian_Drillsma"]
}

variable "CloudWatch_users" {
  description = "Create IAM users with these names"
  type = list(string)
  default = ["Cloudwatch_Reports"]
}

############# IAM Role variables ###############

variable "role_name" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-Admin"
}

variable "policy_location" {
  description = "Define assume Policy location"
  default = "policies/role-trust-policy.json"
}

variable "access_policy" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/AdministratorAccess"
}

variable "role_name_CW" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-CloudWatchReadOnly"
}

variable "access_policy_CW" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}
