provider "aws" {
  region = "eu-west-2"
}

data "aws_organizations_organization" "current" {}

module "s3_state" {
  source = "../../../../modules/s3_bucket"
  bucket-name = var.s3_state_bucket
  policy_doc = null
  enable_logging = false
}






