provider "aws" {
  region = "eu-west-2"
}

module "iam_settings" {
  source      = "../../../../modules/iam_settings"
  min_pass_lenght_module = var.min_pass_lenght
  expiration_days = var.expiration_days
  pass_remember = var.pass_remember
}

module "prod_adminrole" {
  source = "../../../../modules/iam_role"
  role_name = var.role_name
  policy_location = templatefile("policies/role-trust-policy.json", {})
  access_policy = var.access_policy
}

module "CloudWatch-RO" {
  source =  "../../../../modules/iam_role/"
  role_name = var.role_name-CW
  policy_location = templatefile("policies/role-trust-policy.json", {})
  access_policy = var.access_policy-CW
}
