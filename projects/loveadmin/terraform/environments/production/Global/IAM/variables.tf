######### IAM Config #########

variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-2"
}

variable "min_pass_lenght" {
  description = "Minimum lenght of the password"
  default = "8"
}

variable "policy_arn" {
  description = "Managed AWS policy to be attached"
  default     = ""
}

variable "expiration_days" {
  description = "Days before password expire"
  default     = "90"
}

variable "pass_remember" {
   description = "Number of remebered old user's passwords"
   default     = "5"
 }

######### IAM Role Settings #########

variable "role_name" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-Admin"
}

variable "access_policy" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/AdministratorAccess"
}

variable "role_name-CW" {
  type = string
  description = "AWS IAM Role"
  default = "LoveAdmin-CloudWatchReadOnly"
}

variable "access_policy-CW" {
  type = string
  description = "arn of the access policy"
  default = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}
