### uncomment this after you create the bucket and run terraform init again
terraform {
   backend "s3" {
     bucket = "loveadmin-terraform-state-prod"
     key    = "VPC/terraform.tfstate"
     region = "eu-west-2"
   }
 }
