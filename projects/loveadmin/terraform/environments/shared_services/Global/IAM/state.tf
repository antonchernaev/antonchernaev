### uncomment this after you create the bucket and run terraform init again
terraform {
   backend "s3" {
     bucket = "loveadmin-terraform-state-shs"
     key    = "Global/IAM/terraform.tfstate"
     region = "eu-west-2"
   }
 }
