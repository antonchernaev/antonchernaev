### uncomment this after you create the bucket and run terraform init again
terraform {
   backend "s3" {
     bucket = "loveadmin-terraform-state-stg"
     key    = "Global/S3/terraform.tfstate"
     region = "eu-west-2"
   }
 }
