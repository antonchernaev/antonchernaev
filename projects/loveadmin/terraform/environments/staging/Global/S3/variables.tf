variable "s3_state_bucket" {
  description = "The bucket to use for storing terrform state files"
  default = "loveadmin-terraform-state-stg"
}

