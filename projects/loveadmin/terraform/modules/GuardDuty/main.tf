#resource "aws_guardduty_detector" "Detector" {
#  enable = true
#}


// Creates invitation in the master account

resource "aws_guardduty_member" "invite_member" {
  provider    = aws.master
  account_id  = data.aws_caller_identity.member.account_id
  detector_id = var.detector_master_id
  email       = var.email
  invite      = true
}

// Accepts the invitation in the member account

resource "aws_guardduty_invite_accepter" "member_accept" {
  depends_on = [aws_guardduty_member.invite_member]
  provider   = aws.member

  detector_id       = var.detector_member_id
  master_account_id = data.aws_caller_identity.master.account_id
}
