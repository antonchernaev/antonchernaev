// The Guardduty master account.
provider "aws" {
  alias = "master"
}
// The Guardduty member account.
provider "aws" {
  alias = "member"
}

data "aws_caller_identity" "master" {
  provider = aws.master
}

data "aws_caller_identity" "member" {
  provider = aws.member
}
