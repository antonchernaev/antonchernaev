variable "email" {
  type        = string
  description = "Email used for creating AWS account."
}

variable "detector_master_id" {
  type        = string
  description = "GuardDuty master detector ID."
}

variable "detector_member_id" {
  type        = string
  description = "GuardDuty member detector ID."
}
