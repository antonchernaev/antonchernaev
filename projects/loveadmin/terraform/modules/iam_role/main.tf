resource "aws_iam_role" "Role" {
  name = var.role_name
  assume_role_policy = var.policy_location
}

resource "aws_iam_role_policy_attachment" "access_policy" {
  policy_arn = var.access_policy
  role = aws_iam_role.Role.name
}

output "role_arn" {
  value = aws_iam_role.Role.arn
}


