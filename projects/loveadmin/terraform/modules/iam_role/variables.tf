variable "role_name" {
  description = "AWS IAM Role"
}

variable "policy_location" {
  description = "Define Policy location"
}

variable "access_policy" {
  description = "arn of the access policy"
}



