resource "aws_iam_account_password_policy" "strict" {
  minimum_password_length        = var.min_pass_lenght_module
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
  max_password_age               = var.expiration_days
  hard_expiry                    = true
  password_reuse_prevention      = var.pass_remember
}



