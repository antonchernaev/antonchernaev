variable "min_pass_lenght_module" {
  description = "Minimum lenght of the password"
  default = "8"
}

variable "expiration_days" {
  description = "Days before password expire"
  default     = "90"
}

variable "pass_remember" {
   description = "Number of remebered old user's passwords"
   default     = "5"
 }