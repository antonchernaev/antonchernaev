resource "aws_iam_user" "user" {
  count = length(var.user_names)
  name = var.user_names[count.index]
}

resource "aws_iam_group_membership" "GroupMemebership" {
  depends_on = [aws_iam_user.user]
  name = var.iam_group
  group = var.iam_group
  users = var.user_names
}


