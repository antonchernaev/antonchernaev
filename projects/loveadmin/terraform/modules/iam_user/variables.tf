variable "user_names" {
  description = "Provide a user names that you want to create"
  type = list(string)
}

variable "iam_group" {
  description = "Attach the IAM user to provided group name"
}




