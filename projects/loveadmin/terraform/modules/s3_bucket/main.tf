resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket-name
  acl    = var.acl_access_logging
  policy = var.policy_doc

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    prefix  = "/"
    enabled = true

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags = {
    Name       = var.bucket-name
    Deployment = "tf"
  }

  lifecycle {
    prevent_destroy = false
  }

  dynamic "logging" {
    for_each = var.enable_logging  == false ? [] : ["x"]
    content {
      target_bucket = var.bucket_name_s3_access_logs
      target_prefix = var.target_prefix
    }
  }
}


output "aws_s3_bucket" {
  value = aws_s3_bucket.bucket.bucket
}
