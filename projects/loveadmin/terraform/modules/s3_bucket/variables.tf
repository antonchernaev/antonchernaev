variable "bucket-name" {
  type        = string
  description = "The bucket to use for storing terrform state files"
}

variable "policy_doc" {
  description = "The bucket policy to be use for the bucket"
}

variable "enable_logging" {
  type = bool
  default = true
}

variable "bucket_name_s3_access_logs" {
  description = "enable logging on a bucket"
  type =  string
  default = null
}

variable "target_prefix" {
  description = "name of the folder"
  type = string
  default = null
}

variable "acl_access_logging" {
  description = "Allow to other s3 buckets to log events in access logging s3 bucket"
  default = "private"
}

