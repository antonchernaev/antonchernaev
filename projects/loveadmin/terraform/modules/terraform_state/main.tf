provider "aws" {
  region = var.region
}

### uncomment this after you create the bucket and run terraform init again
 terraform {
   backend "s3" {
     bucket = var.s3_state_bucket
#     key = "${path.cwd}/terraform.tfstate"
     key    = "Global/S3/terraform.tfstate"
     region = var.region
   }
 }