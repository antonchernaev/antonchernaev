variable "region" {
  description = "AWS regions to use"
  default = "eu-west-1"
}

variable "s3_state_bucket" {
  description = "Location for terraform state storing"
  default = "s3statebucketloveadmin"
}