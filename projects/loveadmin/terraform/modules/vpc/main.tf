resource "aws_vpc" "default" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name       = var.vpc_name
    Deployment = var.deployment_method
  }
}

### VPC flow logs

resource "aws_flow_log" "flowlog_to_cloudwatch" {
  iam_role_arn   = aws_iam_role.vpc_flowlogs_role.arn
  vpc_id         = aws_vpc.default.id
  log_destination = aws_cloudwatch_log_group.vpc_log_group.arn
  traffic_type   = "ALL"
      lifecycle {
        ignore_changes = [iam_role_arn, vpc_id, traffic_type]
    }
}



resource "aws_flow_log" "flowlog_to_s3" {
#  log_destination = aws_s3_bucket.flowlog_bucket.arn
  log_destination = var.vpc_flowLog_destination
  log_destination_type = "s3"
  traffic_type = "ALL"
  vpc_id = aws_vpc.default.id
}

resource "aws_cloudwatch_log_group" "vpc_log_group" {
  name = var.vpc_cloudwatch_log_group
}

resource "aws_iam_role" "vpc_flowlogs_role" {
  name = var.vpc_flowlogs_role_name

  assume_role_policy = templatefile("${path.module}/policies/vpc_flowlogs_role_assume_policy.json", {})

  lifecycle {
   ignore_changes = [name, assume_role_policy]
 }
}

resource "aws_iam_role_policy" "vpc_flowlogs_policy" {
  name = var.vpc_flowlogs_policy_name
  role = aws_iam_role.vpc_flowlogs_role.id
  policy = templatefile("${path.module}/policies/vpc_flowlogs_role.json", {})
}

################## Custom subnets and Internet Gateway and Availability zones ######

# Declare the data sourcei, whether availabily zone status is available

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "vpc_public_subnet" {
  count = var.number_of_subnets_public
  cidr_block = var.vpc_subnet_cidr_public[count.index]
  vpc_id = aws_vpc.default.id
  map_public_ip_on_launch = var.vpc_subnet_public_private
  availability_zone = data.aws_availability_zones.available.names[count.index] //Availablity zone number in the array of available ones.
  tags = {
    Name = var.subnet_description_public[count.index]
  }
}

resource "aws_subnet" "vpc_private_subnet" {
  count = var.number_of_subnets_private
  cidr_block = var.vpc_subnet_cidr_private[count.index]
  vpc_id = aws_vpc.default.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = {
    Name = var.subnet_description_private[count.index]
  }
}


resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.default.id
    tags = {
        Name = "${var.vpc_name}-igw"
    }
}

resource "aws_eip" "nat_gateway" {
  count = var.number_nat_gateways_private_subnets
  vpc = var.vpc_elasticip
}

resource "aws_nat_gateway" "gw" {
  count = var.number_nat_gateways_private_subnets
  depends_on = [aws_internet_gateway.igw]
  allocation_id = aws_eip.nat_gateway[count.index].id
  subnet_id = var.number_nat_gateways_private_subnets != 1 ? aws_subnet.vpc_private_subnet[count.index].id : aws_subnet.vpc_private_subnet[0].id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = var.route_table_description[0]
  }
}

resource "aws_route_table_association" "public" {
  count = var.number_of_subnets_public
  subnet_id      = aws_subnet.vpc_public_subnet[count.index].id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  count = var.number_nat_gateways_private_subnets
  vpc_id = aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw[count.index].id
  }

  tags = {
    Name = var.route_table_description[1]
  }
}

resource "aws_route_table_association" "private" {
  count = var.number_of_subnets_public
  subnet_id      = aws_subnet.vpc_private_subnet[count.index].id
  route_table_id = aws_route_table.private[var.number_of_subnets_private == var.number_nat_gateways_private_subnets ? count.index : 0].id
}
