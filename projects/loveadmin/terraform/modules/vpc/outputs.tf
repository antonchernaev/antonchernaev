output "vpc_id" {
  value = aws_vpc.default.id
}

output "number_of_available_avl-zones" {
  value = length(data.aws_availability_zones.available)
}

output "vpc_flow_log_group_name" {
  value = aws_flow_log.flowlog_to_cloudwatch.log_group_name
}
