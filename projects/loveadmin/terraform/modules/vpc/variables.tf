variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
}

variable "vpc_name" {
  description = "Default Name of the VPC"
}

variable "deployment_method" {
  description = "Default Name of the VPC"
}

variable "vpc_flowlogs_role_name" {
  description = "Defult name for flowlogs IAM role"
}

variable "vpc_flowlogs_policy_name" {
  description = "Default Name for flowlogs IAM policy"
}

variable "vpc_cloudwatch_log_group" {
  description = "Default Name for the Cloudwatch logs log group"
}

variable "vpc_subnet_cidr_public" {
  description = "CIDR block for vpc network"
}

variable "vpc_subnet_cidr_private" {
  description = "CIDR block for vpc network"
}

variable "vpc_subnet_public_private" {
  description = "Makes a subnet public or private, value true or false"
  default = true
}

variable "subnet_description_private" {
  description = "prod, private, 1 or 2 or .."
}

variable "subnet_description_public" {
  description = "prod, public, 1 or 2 or .."
}

variable "number_of_subnets_public" {
  description = "Number of subnets to create."
  type = number
}

variable "number_of_subnets_private" {
  description = "Number of subnets to create."
  type = number
}

variable "vpc_elasticip" {
  description = "Elastic ip"
  type = bool
}

variable "number_nat_gateways_private_subnets" {
  description = "Number of NAT Gateways"
}

variable "vpc_flowLog_destination" {
  description = "s3 bucket identifier for vpc flow logs"
}

variable "route_table_description" {
  description = "Name of the route tables"
}
